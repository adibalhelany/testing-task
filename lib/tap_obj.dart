class TabObj {
  final int id;
  final String name;
  final String displayName;
  final String icon;
  final String role;

  const TabObj(
      {required this.id,
      required this.name,
      required this.displayName,
      required this.icon,
      required this.role});

  factory TabObj.fromJson(Map<String, dynamic> json) {
    return TabObj(
      id: json['id'],
      name: json['name'],
      displayName: json['displayname'],
      icon: json['icon'],
      role: json['role'],
    );
  }
}
