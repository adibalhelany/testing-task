import 'dart:convert';
import 'package:http/http.dart' as http;
import '../tap_obj.dart';

Future<TabObj> addRoleToTab(int tabId, String role, http.Client client) async {
  http.Response response = await client.put(
    Uri.parse('http://localhost:55005/api/sitemap/$tabId/$role'),
  );

  if (response.statusCode == 200) {
    return TabObj.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load');
  }
}
