import 'dart:convert';
import 'package:http/http.dart' as http;
import '../tap_obj.dart';

Future<TabObj> addRolesToTab(
    int tabId, List<String> roles, http.Client client) async {
  http.Response response = await client.put(
      Uri.parse('http://localhost:55005/api/sitemap/$tabId'),
      body: jsonEncode(roles));

  if (response.statusCode == 200) {
    return TabObj.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load ');
  }
}
