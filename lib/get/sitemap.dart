import 'dart:convert';
import 'package:http/http.dart' as http;
import '../tap_obj.dart';

Future<TabObj> siteMap(http.Client client, List<String> roles) async {
  String X = '';
  roles.forEach((elemnts) {
    X = X + '/$elemnts';
  });
  http.Response response =
      await client.get(Uri.parse('http://localhost:55005/api/sitemap$X'));

  if (response.statusCode == 200) {
    return TabObj.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}
