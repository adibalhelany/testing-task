import 'package:tester/add/add_role_to_tab.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tester/add/add_roles_to_tab.dart';
import 'package:tester/get/sitemap.dart';
import 'package:tester/remove/remove_role_from_tab.dart';
import 'package:tester/remove/remove_roles_from_tap.dart';
import 'package:tester/tap_obj.dart';
import 'add_role_to_tab_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  group('Add Test', () {
    test('Add role to tap', () async {
      final client = MockClient();

      // Use Mockito to return a successful response when it calls the
      // provided http.Client.
      when(client.put(Uri.parse('http://localhost:55005/api/sitemap/1/a')))
          .thenAnswer((_) async => http.Response(
              '{"id": 1, "name": "jhafsjk", "displayname": "mock","icon":"add","role":"role"}',
              200));

      expect(await addRoleToTab(1, 'a', client), isA<TabObj>());
    });
    test('throws an exception if the http call completes with an error', () {
      final client = MockClient();

      when(client.put(Uri.parse('http://localhost:55005/api/sitemap/1/a')))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect(addRoleToTab(1, 'a', client), throwsException);
    });

    test('Add roleS to tap', () async {
      final client = MockClient();

      when(
        client.put(Uri.parse('http://localhost:55005/api/sitemap/1'),
            body: '["a","b","c"]'),
      ).thenAnswer(
        (_) async => http.Response(
            '{"id": 1, "name": "jhafsjk", "displayname": "mock","icon":"add","role":"role"}',
            200),
      );

      expect(await addRolesToTab(1, ['a', 'b', 'c'], client), isA<TabObj>());
    });
  });

  group('Remove Test', () {
    test('Remove role from tap', () async {
      final client = MockClient();

      when(client.delete(Uri.parse('http://localhost:55005/api/sitemap/1/a')))
          .thenAnswer((_) async => http.Response(
              '{"id": 1, "name": "jhafsjk", "displayname": "mock","icon":"add","role":"role"}',
              200));

      expect(await reomveRoleFromTab(1, 'a', client), isA<TabObj>());
    });

    test('Remove roleS from tap', () async {
      final client = MockClient();

      when(
        client.delete(Uri.parse('http://localhost:55005/api/sitemap/1'),
            body: '["a","b","c"]'),
      ).thenAnswer(
        (_) async => http.Response(
            '{"id": 1, "name": "jhafsjk", "displayname": "mock","icon":"add","role":"role"}',
            200),
      );

      expect(
          await removeRolesFromTab(1, ['a', 'b', 'c'], client), isA<TabObj>());
    });

    test('throws an exception if the http call completes with an error', () {
      final client = MockClient();

      when(client.delete(Uri.parse('http://localhost:55005/api/sitemap/1/a')))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect(reomveRoleFromTab(1, 'a', client), throwsException);
    });
  });
  test('Get UI', () async {
    final client = MockClient();

    when(
      client.get(
        Uri.parse('http://localhost:55005/api/sitemap/role0/role1'),
      ),
    ).thenAnswer((_) async => http.Response(
        '{"id": 1, "name": "jhafsjk", "displayname": "mock","icon":"add","role":"role"}',
        200));

    expect(await siteMap(client, ['role0', 'role1']), isA<TabObj>());
  });
}
